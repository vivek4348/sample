/**
 * 
 */
package com.dineshonjava.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dineshonjava.domain.UserRepository;
import com.dineshonjava.model.User;

/**
 * @author Dinesh.Rajput
 *
 */
@Service
public class UserService {
	
	@Autowired
    private UserRepository userRepository;
 
    @Transactional(readOnly=true)
    public List<User> findAll() {
        List<User> findAll = (List<User>) userRepository.findAll();
		return findAll;
    }

    @Transactional(readOnly=false)
	public User create() {
		User user = new User();
		//user.setUserId(5);
		user.setUserName("Vivekanand Alampally");
		user.setUserEmail("vivek4348@gmail.com");
		user.setAddress("2341 Dulles ...");
		User save = userRepository.save(user);
		return save;
	}
}

