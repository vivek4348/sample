/**
 * 
 */
package com.dineshonjava.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
public class User implements Serializable{
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="userId")
	private Integer userId;
	
	@Column(name = "userName", unique = false, nullable = true)
	private String userName;
	
	@Column(name = "userEmail", unique = false, nullable = true)
	private String userEmail;
	
	@Column(name = "address", unique = false, nullable = true)
	private String address;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName
				+ ", userEmail=" + userEmail + ", address=" + address + "]";
	}
	 
}
