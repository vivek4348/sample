package com.dineshonjava;

import org.hsqldb.util.DatabaseManagerSwing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataBaseApplication {

	public static void main(String[] args) {
		System.setProperty("java.awt.headless", "false");
		SpringApplication.run(SpringBootDataBaseApplication.class, args);
		
		DatabaseManagerSwing.main(new String[] { "--url", "jdbc:hsqldb:file:///c:/temp/hsql/one", "--user", "sa", "--password", "" });
	}
	
	/*@PostConstruct
	public void startDBManager() {
		
		//hsqldb
		DatabaseManagerSwing.main(new String[] { "--url", "jdbc:hsqldb:file:///c:/temp/hsql/one", "--user", "sa", "--password", "" });

		//derby
		//DatabaseManagerSwing.main(new String[] { "--url", "jdbc:derby:memory:testdb", "--user", "", "--password", "" });

		//h2
		//DatabaseManagerSwing.main(new String[] { "--url", "jdbc:h2:mem:testdb", "--user", "sa", "--password", "" });

	}*/
	
	
	/*@PostConstruct
	public void startDBM() {
		 //System.setProperty("java.awt.headless", "false");
		MethodInvokingBean mBean = new MethodInvokingBean();

		mBean.setTargetClass(DatabaseManagerSwing.class);
		mBean.setTargetMethod("main");
		String[] args = new String[] { "--url", "jdbc:hsqldb:file:///c:/temp/hsql/one", "--user", "sa", "--password", "" };
		mBean.setArguments(args);
		try {
			mBean.prepare();
			mBean.invoke();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}*/
}
